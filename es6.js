//soal 1. syntax arrow
//javascript biasa
const golden = function goldenFunction(){
    console.log("this is golden!!");
  }
   
  golden();

//es6
const golden2 = () => { return console.log("this is golden es6 !!")};
golden2();

//sola 2. Object Literals
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 

//es6
const namaPanjang = (namaDepan, namaBelakang) => {
    return{
        namaDepan,
        namaBelakang,   
        fullname: () =>{
        console.log(`${namaDepan} ${namaBelakang}`)
        } 
    }
} 
namaPanjang("Olive","Oil").fullname()

//soal 3 Destruction
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
//es 5
const firstName1 = newObject.firstName;
const lastName1 = newObject.lastName;
const destination1 = newObject.destination;
const occupation1 = newObject.occupation;

// Driver code
console.log(firstName1, lastName1, destination1, occupation1)

//es 6
const{ firstName, lastName, destination, occupation} = newObject;
console.log(firstName, lastName, destination, occupation)

//soal 4 Rest & Spread
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
console.log(combined)

//es6
//rest
const bersatu = [...west,...east];
console.log(bersatu);
//spread
let data = ["Tanjiro", "Tamarin"]
let menyebar = (nama,permen) => {
    return {
        nama,
        permen,
        kalimat : () => {
            return `${nama} makan ${permen}`
        }
    }
}
console.log(menyebar(...data).kalimat());

//soal 5 Template Literals
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' +       'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +     'incididunt ut labore et dolore magna aliqua. Ut enim' +     ' ad minim veniam'   
console.log(before) 

//es6 
    console.log(`Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do esiusmod tempor incididunt ut labore et dolore magna aliqua. ut enim ad minim veniam`)
    
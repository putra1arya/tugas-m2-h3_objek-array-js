
const newOb = new Vue({
    el : '#container',
    data : {
        countId : 0,
        content : '',
        date : new Date(),
        komentar : [
            
        ]
    },
    methods :{
        printKomentar(){
            this.komentar.push({
                id : this.countId,
                content : this.content,
                date : this.date,
                score : 0
            });
            this.countId++;
        },
        tambah(id){
            this.komentar[id].score++;
        },
        kurang(id){
            this.komentar[id].score--;
        }
    }
});
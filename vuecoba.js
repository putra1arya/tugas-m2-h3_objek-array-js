const newVue = new Vue({
    el : '#app', //diisi element html yang ingin dimainkan
    data: {
        show : 'readx', 
        nama : "Arya",
        umur : 23,
        raw : '<p>New World</p>',
        massage : 'DEAL WITH ME WORLD',
        angka1 : 5,
        hasil: null,
        kilometers : 0,
        meters: 0,
        textarea : '',
    },
    computed: {
        sum () {
            return this.umur+ this.angka1;
        },
        kali(){
            return this.umur * this.angka1;
        }
        // sum : function() {
        //     return this.umur+ this.angka1;
        // }
    },
    methods : {
        jumlah (angka2) {
            return this.hasil = this.umur + this.angka1 * angka2;
        }
    },
    watch :{
        kilometers(val){
            this.kilometers = val;
            this.meters = val * 1000;
        },
        meters(val){
            this.kilometers = val/1000;
            this.meters = val;
        }
    }
});
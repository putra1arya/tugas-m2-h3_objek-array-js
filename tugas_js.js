//soal 1.
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];

daftarBuah.sort();
for (let i = 0; i < daftarBuah.length; i++) {
    document.write(daftarBuah[i] + '<br>');
}

//soal 2.
var kalimat="saya sangat senang belajar javascript"
var res = kalimat.split(' ');

console.log(res);
document.write(res);

//soal 3. 
document.write('<br><br>');
const buah = {
    strawberry : {
        nama : "strawberry",
        warna : "merah",
        ada_bijinya : "tidak",
        harga : 9000
    },
    jeruk : {
        nama : "jeruk",
        warna : "oranye",
        ada_bijinya : "ada",
        harga : 8000
    },
    semangka : {
        nama : "semangka",
        warna : "Hijau & merah",
        ada_bijinya : "ada",
        harga : 10000
    },
    pisang : {
        nama : "Pisang",
        warna : "kuning",
        ada_bijinya : "tidak",
        harga : 5000
    }
}
document.write("Nama : " + buah.strawberry.nama + "<br>");
document.write("Warna : " + buah.strawberry.warna + "<br>");
document.write("Ada Bijinya : " + buah.strawberry.ada_bijinya + "<br>");
document.write("Harga : " + buah.strawberry.harga + "<br>");
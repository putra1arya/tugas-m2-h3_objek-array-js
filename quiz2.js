
const newOb = new Vue({
    el : '#container',
    data : {
        countId : 0,
        nama : '',
        status: true,
        indeks : '',
        users : [
            {
                name : 'Inugami Korone',
            },
            {
                name : 'Miko Miko',
            },
            {
                name : 'Usada Pekora',
            },
        
        ]
    },
    methods :{
        tambahNama(){
            this.users.push({
                name: this.nama
            });
        },
        updateNama(){
            this.users.forEach((user,index) => {
                if(index == this.indeks){
                    user.name = this.nama;
                }
            });
            this.status = true;
        },
        editBtn(val,ind){
            this.indeks = ind;
            this.nama = val;
            this.status = false;
        },
        hapusBtn(val){
            var jawab = window.confirm("Anda Yakin Menghapus Data ?")
            if(jawab){
                this.users.forEach((user,index) =>{
                    if(user.name == val){
                        this.users.pop(index);
                    }
                })
            }            
        }
    }
});